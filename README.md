# terraform module S3 bucket example

Vous trouverez ici un exemple d'utilisation et de création de bucket comme nous avons pu le voir lors de la formation.

## How to use it?

```bash
terraform init
terraform plan
terraform apply -auto-approve
terraform destroy
```