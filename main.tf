module "storage" {
  source = "./modules/storage"
  bucket_name = var.bucket_name
  aws_profile = "sandbox.terraform.service.account"
}
