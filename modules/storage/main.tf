terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
      }
    }
}
provider "aws" {
  region = "eu-west-1"
  profile = var.aws_profile
}

resource "aws_s3_bucket" "bucket_test" {
  bucket = var.bucket_name
  tags = {
    Identifiant = "monbucket-${var.bucket_name}"
    Environment = "Dev"
    }
}
