variable "bucket_name" {
  type        = string
  description = "le nom du bucket"
}

variable "aws_profile" {
  type        = string
  description = "Le profile aws cli a utiliser pour generer le bucket"
}
